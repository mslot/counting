using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Counting.Services.Core;
using Counting.Services.Core.Options;
using Counting.Services.CounterWriter.Business;
using Counting.Services.CounterWriter.Core;
using Counting.Services.Infrastructure.Mappers;
using Counting.Services.Infrastructure.Services;
using Counting.Services.Infrastructure.Stores;
using EventStore.ClientAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Counting.Services.Writer.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IEventStoreConnection _connection;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            _connection = StartConnection(Configuration);

            services.AddSwaggerGen();
            services.AddSingleton<IEventStore, EventStoreDbStore>();
            services.AddScoped<IEventService, EventService>();
            services.AddSingleton<CommandMapper>();
            services.AddSingleton<IMapper<ICounterCommand, ICounterEvent>, EventMapper>();
            services.AddSingleton(_connection);

            AddConfiguration(services);
        }

        private IEventStoreConnection StartConnection(IConfiguration configuration)
        {
            var eventStoreDbConfig = configuration.GetSection("EventStoreDbOptions").Get<EventStoreDbOptions>();
            var connection = EventStoreConnection.Create(eventStoreDbConfig.ConnectionString);
            Task task = connection.ConnectAsync();
            task.GetAwaiter().GetResult();

            return connection;
        }

        private void AddConfiguration(IServiceCollection services)
        {
            var eventStoreDbConfig = Configuration.GetSection("EventStoreDbOptions").Get<EventStoreDbOptions>();
            services.AddSingleton(eventStoreDbConfig);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Counting writer API");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            applicationLifetime.ApplicationStopped.Register(OnShoutdown);
        }

        private void OnShoutdown()
        {
            _connection.Close();
        }
    }
}
