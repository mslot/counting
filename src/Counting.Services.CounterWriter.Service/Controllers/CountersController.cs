﻿using Counting.Services.CounterWriter.Core;
using Counting.Services.CounterWriter.Core.Commands;
using Counting.Services.Infrastructure.Mappers;
using Counting.Services.Infrastructure.Requests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Counting.Services.Writer.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountersController : ControllerBase
    {
        private readonly IEventService _eventService;
        private readonly CommandMapper _commandMapper;

        public CountersController(
            IEventService eventService, 
            CommandMapper commandMapper)
        {
            _eventService = eventService;
            _commandMapper = commandMapper;
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<IActionResult> New([FromBody] CounterCreateRequest request)
        {
            IMapper<CounterCreateRequest, CreateCounterCommand> createCommandMapper = _commandMapper;
            var command = createCommandMapper.Map(request);
            await _eventService.CreateCounterAsync(command);

            return Accepted();
        }

        [HttpPost("{name}/{operation}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<IActionResult> Update([FromBody] CounterUpdateRequest request, string name, Operation operation)
        {
            var command = _commandMapper.Map(request, operation, name);
            await _eventService.CounterChangedAsync(command);
            return Accepted();
        }
    }
}
