﻿using Counting.Services.CounterWriter.Core.Commands;
using System.Threading.Tasks;

namespace Counting.Services.CounterWriter.Core
{
    public interface IEventService
    {
        Task CreateCounterAsync(CreateCounterCommand command);
        Task CounterChangedAsync(ICounterCommand command);
    }
}
