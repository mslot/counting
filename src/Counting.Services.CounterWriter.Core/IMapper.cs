﻿namespace Counting.Services.CounterWriter.Core
{
    public interface IMapper<T,U>
    {
        U Map(T obj);
    }
}
