﻿using System;

namespace Counting.Services.CounterWriter.Core
{
    public interface ICounterCommand
    {
        string Name { get; }
        int Value { get; }
    }
}
