﻿using System;

namespace Counting.Services.CounterWriter.Core.Commands
{
    public class DecrementCounterCommand : ICounterCommand
    {
        public string Name { get; }
        public int Value { get; }

        public DecrementCounterCommand(string name, int value)
        {
            Name = name;
            Value = value;
        }
    }
}
