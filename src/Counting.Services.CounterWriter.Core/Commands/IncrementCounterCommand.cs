﻿using System;

namespace Counting.Services.CounterWriter.Core.Commands
{
    public class IncrementCounterCommand : ICounterCommand
    {
        public string Name { get; }
        public int Value { get; }

        public IncrementCounterCommand(string name, int value)
        {
            Value = value;
            Name = name;
        }
    }
}
