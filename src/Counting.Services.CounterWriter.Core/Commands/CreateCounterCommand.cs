﻿using System;

namespace Counting.Services.CounterWriter.Core.Commands
{
    public class CreateCounterCommand : ICounterCommand
    {
        public string Name { get; }
        public int InitialValue { get; }
        public int Value { get => InitialValue; }

        public CreateCounterCommand(string name, int value)
        {
            Name = name;
            InitialValue = value;
        }
    }
}
