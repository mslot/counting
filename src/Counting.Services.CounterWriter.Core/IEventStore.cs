﻿using Counting.Services.Core;
using System.Threading.Tasks;

namespace Counting.Services.CounterWriter.Core
{
    public interface IEventStore
    {
        Task AddCounterEventAsync(ICounterEvent @event);
        Task AppendCounterAsync(ICounterEvent @event);
    }
}
