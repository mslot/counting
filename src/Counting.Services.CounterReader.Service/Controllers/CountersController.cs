﻿using Counting.Services.Core;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Counting.Services.CounterReader.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountersController : ControllerBase
    {
        private readonly ICounterReaderStore _readerStore;

        public CountersController(ICounterReaderStore readerStore)
        {
            _readerStore = readerStore;
        }
        [HttpGet("{name}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<IActionResult> Get(string name)
        {
            var counter = await _readerStore.GetAsync(name);

            if (counter == null)
                return NotFound();

            return Ok(counter);
        }
    }
}
