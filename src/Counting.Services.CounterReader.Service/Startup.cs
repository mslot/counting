using Counting.Services.Core;
using Counting.Services.Core.Options;
using Counting.Services.Infrastructure.Stores;
using Counting.Services.Infrastructure.Stores.Mappers;
using Dapper.FluentMap;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Counting.Services.CounterReader.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var counterDatabaseOptions = Configuration.GetSection("CounterDatabaseOptions").Get<CounterDatabaseOptions>();
            services.AddControllers();
            services.AddSwaggerGen();
            services.AddSingleton(counterDatabaseOptions);
            services.AddScoped<ICounterReaderStore, PostgreCounterReaderStore>();

            FluentMapper.Initialize(config =>
            {
                config.AddMap(new CounterMap());
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Counting reader API");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
