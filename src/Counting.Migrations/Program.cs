﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Counting.Migrations.Migrations;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace Counting.Migrations
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var dbOptions = hostContext.Configuration.GetSection("CounterDb").Get<CounterDbOptions>();
                    
                    if (hostContext.HostingEnvironment.EnvironmentName.Equals("DockerLocal")) //This is a bit of a hack, but we want to wait for the database container to be up
                    {
                        Task.Delay(TimeSpan.FromSeconds(30)).GetAwaiter().GetResult();
                    }

                    CreateDababase(dbOptions.BootstrapConnectionString);

                    services.AddFluentMigratorCore()
                            .ConfigureRunner(rb => rb
                                .AddPostgres()
                                .WithGlobalConnectionString(dbOptions.ConnectionString)
                                            .ScanIn(typeof(CreateCounterTable).Assembly).For.Migrations())
                            .AddLogging(lb => lb.AddFluentMigratorConsole());
                    services.AddHostedService<MigrationWorker>();
                });

        private static void CreateDababase(string bootstrapConnectionString)
        {
            var boopstrapConnection = new NpgsqlConnection(bootstrapConnectionString);
            var checkIfDatabaseExists = new NpgsqlCommand(@"SELECT 1 FROM pg_catalog.pg_database WHERE datname = 'counterdatabase'", boopstrapConnection);
            boopstrapConnection.Open();
            int? exist = (int?) checkIfDatabaseExists.ExecuteScalar();

            if(!exist.HasValue)
            {
                var createDatabase = new NpgsqlCommand("CREATE DATABASE counterdatabase", boopstrapConnection);
                createDatabase.ExecuteNonQuery();
            }
            boopstrapConnection.Close();
        }
    }

    public class MigrationWorker : BackgroundService
    {
        private readonly IMigrationRunner _runner;
        private readonly IHostApplicationLifetime _lifeTime;

        public MigrationWorker(IMigrationRunner runner, IHostApplicationLifetime lifeTime)
        {
            _runner = runner;
            _lifeTime = lifeTime;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _runner.MigrateUp();
            _lifeTime.StopApplication();

            return Task.CompletedTask;
        }
    }
}
