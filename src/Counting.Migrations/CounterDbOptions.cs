﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Counting.Migrations
{
    public class CounterDbOptions
    {
        public string ConnectionString { get; set; }
        public string BootstrapConnectionString { get; set; }
    }
}
