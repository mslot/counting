﻿using FluentMigrator;

namespace Counting.Migrations.Migrations
{
    [Migration(202103211510)]
    public class CreateCounterTable : Migration
    {
        public override void Down()
        {
            Delete.Table("counters");
        }

        public override void Up()
        {
            Create.Table("counters")
                    .WithColumn("name")
                    .AsString(255)
                    .PrimaryKey()
                    .WithColumn("value")
                    .AsInt32();
        }
    }
}
