﻿using System.Threading.Tasks;

namespace Counting.Services.Core
{
    public interface ICounterReaderStore
    {
        Task<Counter> GetAsync(string name);
    }
}
