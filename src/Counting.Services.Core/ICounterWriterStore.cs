﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Counting.Services.Core
{
    public interface ICounterWriterStore
    {
        void UpsertCounter(string name, int value);
    }
}
