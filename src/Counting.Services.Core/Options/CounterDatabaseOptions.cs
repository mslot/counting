﻿namespace Counting.Services.Core.Options
{
    public class CounterDatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}
