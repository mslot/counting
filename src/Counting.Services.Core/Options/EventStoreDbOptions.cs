﻿namespace Counting.Services.Core.Options
{
    public class EventStoreDbOptions
    {
        public string ConnectionString { get; set; }
    }
}
