﻿namespace Counting.Services.Core
{
    public class StreamEvent<T>
    {
        public T ReceivedEvent { get; }
        public long CurrentPoint { get; }

        public StreamEvent(T receivedEvent, long currentPoint)
        {
            ReceivedEvent = receivedEvent;
            CurrentPoint = currentPoint;
        }
    }
}
