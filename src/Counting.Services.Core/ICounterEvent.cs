﻿using System;

namespace Counting.Services.Core
{
    public interface ICounterEvent
    {
        Guid Id { get; }
        string Name { get; }
        int Value { get; }
        string EventType { get; }
        DateTimeOffset Timestamp { get; }
    }
}
