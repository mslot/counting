﻿using System;

namespace Counting.Services.Core.Events
{
    public class CounterIncrementedEvent : ICounterEvent
    {
        public Guid Id { get; }
        public string Name { get; }
        public int Value { get; }
        public DateTimeOffset Timestamp { get; }
        public string EventType => GetType().Name;
        public CounterIncrementedEvent(Guid id, string name, int value, DateTimeOffset timestamp)
        {
            Id = id;
            Name = name;
            Value = value;
            Timestamp = timestamp;
        }
    }
}
