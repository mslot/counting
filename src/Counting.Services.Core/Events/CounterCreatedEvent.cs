﻿using System;

namespace Counting.Services.Core.Events
{
    public class CounterCreatedEvent : ICounterEvent
    {
        public Guid Id { get; }
        public string Name { get; }
        public int Value { get; }
        public string EventType => GetType().Name;
        public DateTimeOffset Timestamp { get; }
        public CounterCreatedEvent(Guid id, string name, int value, DateTimeOffset timestamp)
        {
            Id = id;
            Name = name;
            Value = value;
            Timestamp = timestamp;
        }
    }
}
