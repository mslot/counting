﻿using Counting.Services.Core;
using Counting.Services.CounterWriter.Core;
using Counting.Services.CounterWriter.Core.Commands;
using System.Threading.Tasks;

namespace Counting.Services.CounterWriter.Business
{
    public class EventService : IEventService
    {
        private readonly IEventStore _store;
        private readonly IMapper<ICounterCommand, ICounterEvent> _eventMapper;

        public EventService(IEventStore store, IMapper<ICounterCommand, ICounterEvent> eventMapper)
        {
            _store = store;
            _eventMapper = eventMapper;
        }

        public async Task CounterChangedAsync(ICounterCommand command)
        {
            var @event = _eventMapper.Map(command);
            await _store.AppendCounterAsync(@event);
        }

        public async Task CreateCounterAsync(CreateCounterCommand command)
        {
            var @event = _eventMapper.Map(command);
            await _store.AddCounterEventAsync(@event);
        }
    }
}
