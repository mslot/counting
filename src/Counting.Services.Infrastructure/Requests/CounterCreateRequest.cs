﻿namespace Counting.Services.Infrastructure.Requests
{
    public class CounterCreateRequest
    {
        public string Name { get; set; }
        public int InitialValue { get; set; }
    }
}
