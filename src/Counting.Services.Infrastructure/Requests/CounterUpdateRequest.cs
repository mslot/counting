﻿namespace Counting.Services.Infrastructure.Requests
{
    public class CounterUpdateRequest
    {
        public int Value { get; set; }
    }
}
