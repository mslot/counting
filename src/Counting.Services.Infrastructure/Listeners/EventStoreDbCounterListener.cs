﻿using Counting.Services.Core;
using Counting.Services.CounterHandler.Core;
using EventStore.ClientAPI;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Counting.Services.Infrastructure.Listeners
{
    public class EventStoreDbCounterListener : IListener<StreamEvent<ICounterEvent>>
    {
        private readonly IEventStoreConnection _connection;
        private readonly ILogger<EventStoreDbCounterListener> _logger;
        private readonly IConstructor<ICounterEvent> _eventConstructor;

        public EventStoreDbCounterListener(
            IEventStoreConnection connection,
            ILogger<EventStoreDbCounterListener> logger,
            IConstructor<ICounterEvent> eventConstructor)
        {
            _connection = connection;
            _logger = logger;
            _eventConstructor = eventConstructor;
        }

        public void StartListening(Action<StreamEvent<ICounterEvent>> eventReceived, long lastCheckpoint)
        {
            var settings = new CatchUpSubscriptionSettings(
                            maxLiveQueueSize: 10000,
                            readBatchSize: 500,
                            verboseLogging: false,
                            resolveLinkTos: true,
                            subscriptionName: "counterSubscription"
            );

            long? checkpoint = null;
            if (lastCheckpoint != 0)
                checkpoint = lastCheckpoint;

            _connection.SubscribeToStreamFrom(
                stream: "$ce-counter",
                lastCheckpoint: checkpoint,
                settings: settings,
                eventAppeared: (s, r) =>
                {
                    var number = r.OriginalEventNumber;
                    var data = r.Event.Data;
                    var identifier = r.Event.EventType;

                    var constructedEvent = _eventConstructor.Construct(identifier, data);

                    eventReceived(new StreamEvent<ICounterEvent>(constructedEvent, number));

                    return Task.CompletedTask;
                },
                liveProcessingStarted: (s) => _logger.LogInformation("Live processing started"),
                subscriptionDropped: (s, d, e) => _logger.LogInformation("Subscription dropped")
            );
        }
    }
}
