﻿using Counting.Services.CounterHandler.Core;
using System.IO;

namespace Counting.Services.Infrastructure.Synchronizers
{
    public class MemoryFootprintFileSynchronizer : IMemoryFootprintSynchronizer
    {
        private readonly string _path;

        public MemoryFootprintFileSynchronizer(string path)
        {
            _path = path;
        }

        public MemoryFootprint Load()
        {
            using (Stream stream = File.Open(_path, FileMode.OpenOrCreate))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                
                if (stream.Length == 0)
                    return new MemoryFootprint();

                return (MemoryFootprint)binaryFormatter.Deserialize(stream);
            }
        }

        public void Save(MemoryFootprint footprint)
        {
            using (Stream stream = File.Open(_path, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, footprint);
            }
        }
    }
}
