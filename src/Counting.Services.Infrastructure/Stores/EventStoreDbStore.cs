﻿using Counting.Services.Core;
using Counting.Services.CounterWriter.Core;
using EventStore.ClientAPI;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Counting.Services.Infrastructure.Services
{
    public class EventStoreDbStore : IEventStore
    {
        private readonly IEventStoreConnection _connection;

        public EventStoreDbStore(IEventStoreConnection connection)
        {
            _connection = connection;
        }
        public async Task AddCounterEventAsync(ICounterEvent @event)
        {
            string data = JsonSerializer.Serialize(@event);
            string streamName = $"counter-{@event.Name}";
            var eventPayload = new EventData(
                                        eventId: @event.Id,
                                        type: @event.EventType,
                                        isJson: true,
                                        data: Encoding.UTF8.GetBytes(data),
                                        metadata: Encoding.UTF8.GetBytes("{ }")
                                    );

            await _connection.AppendToStreamAsync(streamName, ExpectedVersion.Any, eventPayload);
        }

        public async Task AppendCounterAsync(ICounterEvent @event)
        {
            await AddCounterEventAsync(@event);
        }
    }
}
