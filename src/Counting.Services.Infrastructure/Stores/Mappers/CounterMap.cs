﻿using Counting.Services.Core;
using Dapper.FluentMap.Mapping;

namespace Counting.Services.Infrastructure.Stores.Mappers
{
    public class CounterMap : EntityMap<Counter>
    {
        public CounterMap()
        {
            Map(p => p.Name)
                .ToColumn("name");

            Map(p => p.Value)
                .ToColumn("Value");
        }
    }
}
