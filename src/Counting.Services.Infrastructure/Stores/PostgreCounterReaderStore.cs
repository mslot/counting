﻿using Counting.Services.Core;
using Counting.Services.Core.Options;
using Counting.Services.Infrastructure.Stores.Mappers;
using Dapper;
using Dapper.FluentMap;
using Npgsql;
using System.Threading.Tasks;

namespace Counting.Services.Infrastructure.Stores
{
    public class PostgreCounterReaderStore : ICounterReaderStore
    {
        private readonly CounterDatabaseOptions _options;

        public PostgreCounterReaderStore(CounterDatabaseOptions options)
        {
            _options = options;
        }

        public async Task<Counter> GetAsync(string name)
        {
            using (var connection = new NpgsqlConnection(_options.ConnectionString))
            {
                var counter = await connection.QueryFirstOrDefaultAsync<Counter>("SELECT name, value FROM counters WHERE name = @name", new { name });
                return counter;
            }
        }
    }
}
