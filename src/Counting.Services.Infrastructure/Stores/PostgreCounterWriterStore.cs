﻿using Counting.Services.Core;
using Counting.Services.Core.Options;
using Dapper;
using Npgsql;

namespace Counting.Services.Infrastructure.Stores
{
    public class PostgreCounterWriterStore : ICounterWriterStore
    {
        private readonly CounterDatabaseOptions _options;

        public PostgreCounterWriterStore(CounterDatabaseOptions options)
        {
            _options = options;
        }

        public void UpsertCounter(string name, int value)
        {
            using (var connection = new NpgsqlConnection(_options.ConnectionString))
            {
                connection.Execute(@"INSERT INTO counters (name, value) 
                                     VALUES(@name, @value) 
                                     ON CONFLICT (name) 
                                     DO UPDATE SET value = Excluded.value", new { name, value });
            }
        }
    }
}
