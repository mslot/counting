﻿using Counting.Services.Core;
using Counting.Services.Core.Events;
using Counting.Services.CounterWriter.Core;
using Counting.Services.CounterWriter.Core.Commands;
using System;

namespace Counting.Services.Infrastructure.Mappers
{
    public class EventMapper : IMapper<ICounterCommand, ICounterEvent>
    {
        ICounterEvent IMapper<ICounterCommand, ICounterEvent>.Map(ICounterCommand obj)
        {
            if (obj is IncrementCounterCommand)
            {
                return new CounterIncrementedEvent(Guid.NewGuid(), obj.Name, obj.Value, DateTimeOffset.UtcNow);
            }
            else if (obj is DecrementCounterCommand)
            {
                return new CounterDecrementedEvent(Guid.NewGuid(), obj.Name, obj.Value, DateTimeOffset.UtcNow);
            }
            else
            {
                return new CounterCreatedEvent(Guid.NewGuid(), obj.Name, obj.Value, DateTimeOffset.UtcNow);
            }
        }
    }
}
