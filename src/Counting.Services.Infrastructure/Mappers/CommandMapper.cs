﻿using Counting.Services.CounterWriter.Core;
using Counting.Services.CounterWriter.Core.Commands;
using Counting.Services.Infrastructure.Requests;

namespace Counting.Services.Infrastructure.Mappers
{
    public class CommandMapper : IMapper<CounterCreateRequest, CreateCounterCommand>
    {
        CreateCounterCommand IMapper<CounterCreateRequest, CreateCounterCommand>.Map(CounterCreateRequest obj)
        {
            return new CreateCounterCommand(obj.Name, obj.InitialValue);
        }

        public ICounterCommand Map(CounterUpdateRequest request, Operation operation, string name)
        {
            ICounterCommand command = null;
            switch(operation)
            {
                case Operation.Add:
                    command = new IncrementCounterCommand(name, request.Value);
                    break;
                case Operation.Substract:
                    command = new DecrementCounterCommand(name, request.Value);
                    break;
            }

            return command;
        }
    }
}
