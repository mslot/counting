﻿using Counting.Services.Core;
using Counting.Services.CounterHandler.Core;
using System.Threading.Tasks;

namespace Counting.Services.CounterHandler.Business
{
    public class CounterHandler : ICounterHandler
    {
        private readonly IListener<StreamEvent<ICounterEvent>> _counterListener;
        private readonly Calculator _calculator;
        private readonly EventIdentifier _eventIdentifier;
        private readonly IMemoryFootprintSynchronizer _synchronizer;
        private readonly ICounterWriterStore _counterWriterStore;

        public CounterHandler(IListener<StreamEvent<ICounterEvent>> counterListener,
            Calculator calculator,
            EventIdentifier eventIdentifier,
            IMemoryFootprintSynchronizer synchronizer,
            ICounterWriterStore counterWriterStore)
        {
            _counterListener = counterListener;
            _calculator = calculator;
            _eventIdentifier = eventIdentifier;
            _synchronizer = synchronizer;
            _counterWriterStore = counterWriterStore;
        }
        public Task StartAsync()
        {
            _counterListener.StartListening((e) =>
            {
                var @event = e.ReceivedEvent;
                var currentPoint = e.CurrentPoint;
                var action = _eventIdentifier.Resolve(@event.EventType);

                _calculator.Calculate(@event.Name, currentPoint, action, @event.Value);
                _calculator.MemoryPrint();

            },
            _calculator.Checkpoint);

            return Task.CompletedTask;
        }

        public Task StopAsync()
        {
            return Task.CompletedTask;
        }

        public void Synchronize()
        {
            var footprint = _calculator.GetFootprint();
            _synchronizer.Save(footprint);

            foreach (var cell in footprint.Cells.Values)
            {
                _counterWriterStore.UpsertCounter(cell.Name, cell.Value);
            }
        }
    }
}
