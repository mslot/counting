﻿using Counting.Services.Core;
using Counting.Services.Core.Events;
using Counting.Services.CounterHandler.Core;
using System.Text.Json;

namespace Counting.Services.CounterHandler.Business
{
    public class EventConstructor : IConstructor<ICounterEvent>
    {
        private readonly EventIdentifier _identifier;

        public EventConstructor(EventIdentifier identifier)
        {
            _identifier = identifier;
        }

        public ICounterEvent Construct(string identifier, byte[] data)
        {
            var identifiedType = _identifier.Resolve(identifier);
            ICounterEvent constructedEvent = null;

            switch(identifiedType)
            {
                case CounterActions.Created:
                    constructedEvent = JsonSerializer.Deserialize<CounterCreatedEvent>(data);
                    break;
                case CounterActions.Decrement:
                    constructedEvent = JsonSerializer.Deserialize<CounterDecrementedEvent>(data);
                    break;
                case CounterActions.Increment:
                    constructedEvent = JsonSerializer.Deserialize<CounterIncrementedEvent>(data);
                    break;
            }

            return constructedEvent;
        }
    }
}
