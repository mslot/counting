﻿using Counting.Services.Core;
using Counting.Services.Core.Events;
using Counting.Services.CounterHandler.Core;

namespace Counting.Services.CounterHandler.Business
{
    public class EventIdentifier : IIdentifier<CounterActions>
    {
        public CounterActions Resolve(string identifier)
        {
            CounterActions identifiedType = CounterActions.Created;
            switch (identifier)
            {
                case nameof(CounterCreatedEvent):
                    identifiedType = CounterActions.Created;
                    break;
                case nameof(CounterDecrementedEvent):
                    identifiedType = CounterActions.Decrement;
                    break;
                case nameof(CounterIncrementedEvent):
                    identifiedType = CounterActions.Increment;
                    break;
            }

            return identifiedType;
        }
    }
}
