using Counting.Services.Core;
using Counting.Services.Core.Options;
using Counting.Services.CounterHandler.Business;
using Counting.Services.CounterHandler.Core;
using Counting.Services.CounterHandler.Service.BackgroundServices;
using Counting.Services.Infrastructure.Listeners;
using Counting.Services.Infrastructure.Stores;
using Counting.Services.Infrastructure.Synchronizers;
using EventStore.ClientAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Counting.Services.CounterHandler.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IEventStoreConnection _connection;

        public void ConfigureServices(IServiceCollection services)
        {
            var counterWriterDatabaseOptions = Configuration.GetSection("CounterDatabaseOptions").Get<CounterDatabaseOptions>();
            services.AddControllers();

            _connection = StartConnection(Configuration);

            services.AddSingleton(_connection);
            services.AddSingleton<EventIdentifier>();
            services.AddSingleton((s) => 
            {
                var synchronzier = s.GetRequiredService<IMemoryFootprintSynchronizer>();
                var logger = s.GetRequiredService<ILogger<Calculator>>();
                return new Calculator(synchronzier.Load(), logger);
            });
            services.AddSingleton<IMemoryFootprintSynchronizer>((s) => 
            {
                string path = "./memory_footprint.bin";
                return new MemoryFootprintFileSynchronizer(path);
            });
            services.AddSingleton<IListener<StreamEvent<ICounterEvent>>, EventStoreDbCounterListener>();
            services.AddSingleton<IConstructor<ICounterEvent>, EventConstructor>();
            services.AddScoped<ICounterWriterStore, PostgreCounterWriterStore>();
            services.AddSingleton<ICounterHandler, Business.CounterHandler>();
            services.AddSingleton(counterWriterDatabaseOptions);

            services.AddHostedService<CounterBackgroundService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            applicationLifetime.ApplicationStopped.Register(OnShoutdown);
        }

        private void OnShoutdown()
        {
            _connection.Close();
        }

        private IEventStoreConnection StartConnection(IConfiguration configuration)
        {
            var eventStoreDbConfig = configuration.GetSection("EventStoreDbOptions").Get<EventStoreDbOptions>();
            var connection = EventStoreConnection.Create(eventStoreDbConfig.ConnectionString);
            Task task = connection.ConnectAsync();
            task.GetAwaiter().GetResult();

            return connection;
        }
    }
}
