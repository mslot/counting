﻿using Counting.Services.CounterHandler.Core;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Counting.Services.CounterHandler.Service.BackgroundServices
{
    public class CounterBackgroundService : IHostedService, IDisposable
    {

        private Timer _timer;
        private readonly ICounterHandler _handler;

        public CounterBackgroundService(ICounterHandler handler)
        {
            _handler = handler;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(Synchronize, null, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1));


            return _handler.StartAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return _handler.StopAsync();
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        private void Synchronize(object state)
        {
            _handler.Synchronize();
        }
    }
}
