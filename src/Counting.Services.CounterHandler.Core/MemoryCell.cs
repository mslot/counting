﻿using System;

namespace Counting.Services.CounterHandler.Core
{
    [Serializable]
    public class MemoryCell
    {
        public int Value { get; }
        public string Name { get; }
        public MemoryCell(string name, int value)
        {
            Name = name;
            Value = value;
        }
    }
}
