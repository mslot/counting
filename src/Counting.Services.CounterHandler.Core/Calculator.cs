﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Counting.Services.CounterHandler.Core
{
    //REMARK: Sorry for this :( - this is a mess. I can't seem to find a way in .NET Core to serialize a concurrent  dictionary. Therefore the lock + ran out of time to beautify this
    public class Calculator
    {
        public long Checkpoint { get; private set; }
        private static readonly object _lock = new object();
        private readonly ILogger<Calculator> _logger;
        private ConcurrentDictionary<string, MemoryCell> _memory = new ConcurrentDictionary<string, MemoryCell>();

        public Calculator(MemoryFootprint footprint, ILogger<Calculator> logger)
        {
            Checkpoint = footprint.Checkpoint;
            _memory = new ConcurrentDictionary<string, MemoryCell>(footprint.Cells);
            _logger = logger;
        }

        public MemoryCell Calculate(string name, long checkPoint, CounterActions action, int value)
        {
            lock (_lock)
            {
                var memoryPoint = _memory.AddOrUpdate(name, new MemoryCell(name, value), (key, oldValue) =>
                {
                    var cell = new MemoryCell(name, value);
                    if (action == CounterActions.Created || action == CounterActions.Increment)
                        cell = new MemoryCell(name, Addition(value, oldValue.Value));
                    else
                        cell = new MemoryCell(name, Substraction(value, oldValue.Value));

                    Checkpoint = checkPoint;
                    return cell;
                });

                return memoryPoint;
            }
        }

        public void MemoryPrint()
        {
            foreach (var pair in _memory)
            {
                _logger.LogInformation($"{pair.Key} \t = \t {pair.Value.Value} [{Checkpoint}]");
            }
        }

        public MemoryFootprint GetFootprint()
        {
            lock (_lock)
            {
                return new MemoryFootprint(_memory.ToDictionary(kvp => kvp.Key,
                                                              kvp => kvp.Value), Checkpoint);
            }
        }

        private int Substraction(int value, int oldValue)
        {
            return oldValue - value;
        }

        private int Addition(int value, int oldValue)
        {
            return oldValue + value;
        }
    }
}
