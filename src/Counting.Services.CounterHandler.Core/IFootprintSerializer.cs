﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Counting.Services.CounterHandler.Core
{
    public interface IMemoryFootprintSynchronizer
    {
        void Save(MemoryFootprint footprint);
        MemoryFootprint Load();
    }
}
