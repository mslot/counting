﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Counting.Services.CounterHandler.Core
{
    public interface IIdentifier<T>
    {
        T Resolve(string identifier);
    }
}
