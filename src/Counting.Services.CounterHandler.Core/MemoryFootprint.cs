﻿using System;
using System.Collections.Generic;

namespace Counting.Services.CounterHandler.Core
{
    [Serializable]
    public class MemoryFootprint
    {
        public Dictionary<string, MemoryCell> Cells { get; }
        public long Checkpoint { get; }

        public MemoryFootprint(Dictionary<string, MemoryCell> cells, long checkpoint)
        {
            Cells = cells;
            Checkpoint = checkpoint;
        }

        public MemoryFootprint()
        {
            Cells = new Dictionary<string, MemoryCell>();
            Checkpoint = 0;
        }
    }
}
