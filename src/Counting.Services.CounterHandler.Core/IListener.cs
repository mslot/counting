﻿using System;

namespace Counting.Services.CounterHandler.Core
{
    public interface IListener<T>
    {
        void StartListening(Action<T> eventReceived, long checkpoint);
    }
}
