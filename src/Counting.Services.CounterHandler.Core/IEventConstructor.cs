﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Counting.Services.CounterHandler.Core
{
    public interface IConstructor<T>
    {
        T Construct(string identifier, byte[] data);
    }
}
