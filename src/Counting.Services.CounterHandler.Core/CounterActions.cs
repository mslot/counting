﻿namespace Counting.Services.CounterHandler.Core
{
    public enum CounterActions
    {
        Created,
        Increment,
        Decrement
    }
}
