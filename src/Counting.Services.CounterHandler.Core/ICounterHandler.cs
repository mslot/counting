﻿using System.Threading.Tasks;

namespace Counting.Services.CounterHandler.Core
{
    public interface ICounterHandler
    {
        Task StartAsync();
        Task StopAsync();
        void Synchronize();
    }
}
